create table if not exists "user" (
    id varchar(27) primary key,
    user_name varchar(90) unique not null,
    full_name varchar(90) not null,
    "role" int not null default 1,
    created_at timestamptz not null default now(),
    deleted_at timestamptz
);

create table if not exists email (
    id varchar(27) primary key,
    "uid" varchar(27) not null,
    email varchar(180) unique not null,
    "status" int not null default 10,
    created_at timestamptz not null default now(),
    deleted_at timestamptz,
    constraint fk_email_user foreign key ("uid") 
    references "user"(id) on delete cascade
);
create table if not exists password (
    id varchar(27) primary key,
    "uid" varchar(27) not null,
    "password" varchar(60) not null,
    created_at timestamptz not null default now(),
    deleted_at timestamptz,
    constraint fk_password_user foreign key ("uid") 
    references "user"(id) on delete cascade
);
create table if not exists blogpost (
    id varchar(27) primary key,
    title varchar(160) not null,
    content text not null,
    "status" int not null default 0, 
    author varchar(27),
    published_date timestamptz,
    created_date timestamptz not null default now(),
    constraint fk_blogpost_author foreign key (author) 
    references "user"(id) on delete cascade
);
create table if not exists tag (
    id varchar(27) primary key,
    label varchar(30) unique not null
);
create table if not exists post_tag (
    id varchar(27) primary key,
    tag_id varchar(27),
    post_id varchar(27),
    constraint fk_post_tag foreign key (tag_id) 
    references tag(id) on delete cascade,
    constraint fk_tag_post foreign key (post_id) 
    references blogpost(id) on delete cascade
);

