INSERT INTO public."user" (id,user_name,full_name,"role",created_at,deleted_at) VALUES
	 ('2hM6Ri9peKgNdcPZJRLb5tSIGUW','superuser','user admin',0,'2024-06-03 12:13:12.985225+07',NULL) on conflict do nothing;

INSERT INTO public.email (id,uid,email,status,created_at,deleted_at) VALUES
	 ('2hM6RkcVGBcXEwAzEVEMRZloCic','2hM6Ri9peKgNdcPZJRLb5tSIGUW','admin@email.com',10,'2024-06-03 12:13:12.986794+07',NULL) on conflict do nothing;

INSERT INTO public."password" (id,uid,"password",created_at,deleted_at) VALUES
	 ('2hM6RqRfiOxsm1BjzI9UajH2k6P','2hM6Ri9peKgNdcPZJRLb5tSIGUW','$2a$10$iFQY2/YtXFw7kbgqWLuSm.Z2U5S83qAe47Cx1BkzeDcuZyCP5wahO','2024-06-03 12:13:12.985225+07',NULL) on conflict do nothing;
