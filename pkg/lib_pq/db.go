package lib_pq

import (
	"database/sql"
	"embed"
	"fmt"
	"log"
	"strings"
	"time"

	_ "github.com/lib/pq"
	dotenv "gitlab.com/rmsubekti/blogo/pkg/dot_env"
)

//go:embed queries
var queries embed.FS

type Conn struct {
	*sql.DB
}

func (c *Conn) Connect(env dotenv.Env) (err error) {
	for i := 0; i < 10; i++ {
		c.DB, err = sql.Open("postgres", env.PgDsn())
		if err == nil {
			log.Println("\nconnected to database")
			break
		} else {
			log.Println("\nReconnecting to your database host....")
		}
		time.Sleep(4 * time.Second)
	}
	if err != nil {
		log.Fatalf("No database found : %s", err)
	}
	return c.DB.Ping()
}

func (c *Conn) ExecFile(fileName string) (err error) {
	if sql, err := queries.ReadFile("queries/" + strings.Trim(fileName, " ")); err == nil {
		if _, err := c.Exec(string(sql)); err != nil {
			return fmt.Errorf("error executing file %s : %s", fileName, err.Error())
		}
	}
	return
}
