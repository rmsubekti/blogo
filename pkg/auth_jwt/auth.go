package auth_jwt

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/rmsubekti/blogo/core/port"
	"gitlab.com/rmsubekti/blogo/pkg"
)

func NewToken(u *port.UserInfo, expireDay int) (string, error) {
	// Create the Claims
	claims := &jwt.RegisteredClaims{
		ExpiresAt: jwt.NewNumericDate(time.Now().Add((24 * time.Duration(expireDay)) * time.Hour)),
		IssuedAt:  jwt.NewNumericDate(time.Now()),
		ID:        u.ID,
		Subject:   u.UserName,
		Issuer:    fmt.Sprint(u.Role),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString([]byte(pkg.Env.JWT_SECRET))
}

func Verify(tokenString string) (any, error) {
	user := port.UserInfo{}
	token := strings.SplitN(tokenString, " ", 2)

	if (len(token) != 2) && (token[0] != "Bearer") {
		return nil, errors.New("incorrect format authorization header")
	}

	key, err := jwt.ParseWithClaims(token[1], &jwt.RegisteredClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(pkg.Env.JWT_SECRET), nil
	})

	if claim, ok := key.Claims.(*jwt.RegisteredClaims); ok && key.Valid {
		role, _ := strconv.Atoi(claim.Issuer)
		user.ID = claim.ID
		user.UserName = claim.Subject
		user.Role = uint(role)
		return user, err
	}
	return user, err
}
