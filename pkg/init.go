package pkg

import (
	"log"

	"gitlab.com/rmsubekti/blogo/pkg/dot_env"
	"gitlab.com/rmsubekti/blogo/pkg/lib_pq"
)

var (
	Conn lib_pq.Conn
	Env  dot_env.Env
)

func init() {
	Env.Load()
	if err := Conn.Connect(Env); err != nil {
		log.Fatal("error connecting to database : " + err.Error())
	}
	if err := Conn.ExecFile("tables.sql"); err != nil {
		log.Println("error executing file : " + err.Error())
	}
	if err := Conn.ExecFile("dummy.sql"); err != nil {
		log.Println("error executing file : " + err.Error())
	}
}
