package dot_env

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
)

type Env struct {
	POSTGRES_USER        string
	POSTGRES_PASSWORD    string
	POSTGRES_DB          string
	POSTGRES_HOSTNAME    string
	POSTGRES_PORT        string
	POSTGRES_SSLMODE     string
	POSTGRES_AUTH_METHOD string
	LOG_LEVEL            string
	PORT                 string
	HOSTNAME             string
	JWT_SECRET           string
}

func (e *Env) Load() {
	godotenv.Load()
	e.POSTGRES_USER = get("POSTGRES_USER", "blogo")
	e.POSTGRES_PASSWORD = get("POSTGRES_PASSWORD", "blogo-00BC7ddAa54cpB")
	e.POSTGRES_DB = get("POSTGRES_DB", "blogo")
	e.POSTGRES_HOSTNAME = get("POSTGRES_HOSTNAME", "localhost")
	e.POSTGRES_PORT = get("POSTGRES_PORT", "5432")
	e.POSTGRES_SSLMODE = get("POSTGRES_SSLMODE", "disable")
	e.POSTGRES_AUTH_METHOD = get("POSTGRES_AUTH_METHOD", "trust")

	e.LOG_LEVEL = get("LOG_LEVEL", "debug") // debug || release
	e.PORT = get("PORT", "2011")
	e.HOSTNAME = get("HOSTNAME", "localhost")
	e.JWT_SECRET = get("JWT_SECRET", "Aa8*0$@jaPDooxZOp77){a&x}ttY9")
}

func (e Env) PgDsn() string {
	return fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=%s TimeZone=Asia/Jakarta",
		e.POSTGRES_HOSTNAME, e.POSTGRES_USER, e.POSTGRES_PASSWORD, e.POSTGRES_DB, e.POSTGRES_PORT, e.POSTGRES_SSLMODE)
}

func (e Env) PgUri() (uri string) {
	return fmt.Sprintf("postgres://%s:%s@%s:%s/%s",
		e.POSTGRES_USER, e.POSTGRES_PASSWORD, e.POSTGRES_HOSTNAME, e.POSTGRES_PORT, e.POSTGRES_DB)
}

func get(key, defaultVal string) string {
	v := os.Getenv(key)
	if len(v) > 0 {
		return v
	}
	return defaultVal
}
