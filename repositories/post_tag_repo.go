package repositories

import (
	"context"
	"database/sql"
	"fmt"

	"gitlab.com/rmsubekti/blogo/core/domain"
	"gitlab.com/rmsubekti/blogo/core/port"
)

type postTagRepo struct {
	DB *sql.Tx
}

func NewPostTagRepo(db *sql.Tx) port.IPostTagRepo {
	return &postTagRepo{DB: db}
}

func (r *postTagRepo) GetAssociationByPostId(ctx context.Context, postId string) (out []domain.PostTag, err error) {
	var rows *sql.Rows

	if rows, err = r.DB.QueryContext(ctx, `
		select id, tag_id, post_id from post_tag where post_id = $1 
	`, postId); err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		var pt domain.PostTag
		if err = rows.Scan(&pt.ID, &pt.TagId, &pt.PostId); err != nil {
			return
		}
		out = append(out, pt)
	}
	return
}

func (r *postTagRepo) CreateAssociations(ctx context.Context, in []domain.PostTag) (err error) {
	stmn := "insert into post_tag(id, tag_id, post_id) values "

	for i, v := range in {
		if i != 0 {
			stmn += ","
		}
		stmn += fmt.Sprintf("('%s','%s','%s')", v.ID, v.TagId, v.PostId)
	}
	_, err = r.DB.ExecContext(ctx, stmn)
	return
}

func (r *postTagRepo) DeleteAssociations(ctx context.Context, in []domain.PostTag) (err error) {
	var ids string
	for i, v := range in {
		if i != 0 {
			ids += ","
		}
		ids += fmt.Sprintf("'%s'", v.ID)
	}
	stmn := fmt.Sprintf("delete from post_tag where id in (%s);", ids)
	_, err = r.DB.ExecContext(ctx, stmn)
	return
}
