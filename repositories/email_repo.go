package repositories

import (
	"context"
	"database/sql"

	"gitlab.com/rmsubekti/blogo/core/domain"
	"gitlab.com/rmsubekti/blogo/core/port"
)

type emailRepo struct {
	DB *sql.Tx
}

func NewEmailRepo(db *sql.Tx) port.IEmailRepo {
	return &emailRepo{DB: db}
}

func (r *emailRepo) Create(ctx context.Context, email domain.Email) (err error) {
	_, err = r.DB.ExecContext(ctx, `
		insert into email (id, uid, email,created_at) values ($1, $2, $3, $4)
	`, email.ID, email.UID, email.Email, email.CreatedAt)
	return
}
func (r *emailRepo) IsUsed(ctx context.Context, email string) bool {
	res, err := r.DB.ExecContext(ctx, `
		select * from email e where e.email = $1 
	`, email)

	if err != nil {
		return true
	}

	if v, _ := res.RowsAffected(); v > 0 {
		return true
	}
	return false
}

func (r *emailRepo) GetLoginInfo(ctx context.Context, email string) (out port.LoginInfo, err error) {
	err = r.DB.QueryRowContext(ctx, `
		select u.id, u.user_name, u.full_name,u."role",p.password
		from "user" u join password p on p."uid" = u.id
		join email e on e."uid" = u.id
		where e.email = $1 and p.deleted_at is null
	`, email).Scan(&out.ID, &out.UserName, &out.FullName, &out.Role, &out.Password)
	return
}
