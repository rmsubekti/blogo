package repositories

import (
	"context"
	"database/sql"

	"gitlab.com/rmsubekti/blogo/core/domain"
	"gitlab.com/rmsubekti/blogo/core/port"
)

type passwordRepo struct {
	DB *sql.Tx
}

func NewPasswordRepo(db *sql.Tx) port.IPasswordRepo {
	return &passwordRepo{DB: db}
}
func (r *passwordRepo) SoftDelete(ctx context.Context, passID string) (err error) {
	_, err = r.DB.ExecContext(ctx, "update password set deleted_at=now() where id=$1", passID)
	return
}
func (r *passwordRepo) Create(ctx context.Context, in domain.Password) (err error) {
	_, err = r.DB.ExecContext(ctx, `
		insert into password(id, uid, password) values($1,$2,$3)
	`, in.ID, in.UID, in.Password)
	return
}
func (r *passwordRepo) GetAllByUID(ctx context.Context, uid string) (out []domain.Password, err error) {
	var rows *sql.Rows
	if rows, err = r.DB.QueryContext(ctx, `
		select id, uid, password, created_at, deleted_at
		from password where uid = $1 
	`, uid); err != nil {
		return
	}

	defer rows.Close()

	for rows.Next() {
		var pass domain.Password
		if err = rows.Scan(&pass.ID, &pass.UID, &pass.Password, &pass.CreatedAt, &pass.DeletedAt); err != nil {
			return
		}
		out = append(out, pass)
	}
	return
}
