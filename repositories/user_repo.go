package repositories

import (
	"context"
	"database/sql"

	"gitlab.com/rmsubekti/blogo/core/domain"
	"gitlab.com/rmsubekti/blogo/core/port"
)

type userRepo struct {
	DB *sql.Tx
}

func NewUserRepo(db *sql.Tx) port.IUserRepo {
	return &userRepo{DB: db}
}
func (r *userRepo) Create(ctx context.Context, in domain.User) (err error) {
	_, err = r.DB.ExecContext(ctx, `
		insert into "user"(id, user_name, full_name) values($1,$2,$3) 
	`, in.ID, in.UserName, in.FullName)
	return
}
func (r *userRepo) GetLoginInfo(ctx context.Context, username string) (out port.LoginInfo, err error) {
	err = r.DB.QueryRowContext(ctx, `
		select u.id, u.user_name, u.full_name,u."role",p.password
		from "user" u join password p on p."uid" = u.id
		where u.user_name = $1 and p.deleted_at is null
	`, username).Scan(&out.ID, &out.UserName, &out.FullName, &out.Role, &out.Password)
	return
}
func (r *userRepo) GetById(ctx context.Context, uid string) (out port.UserInfo, err error) {
	err = r.DB.QueryRowContext(ctx, `
		select u.id, u.user_name, u.full_name,u."role" from "user" u
		where u.id=$1 
	`, uid).Scan(&out.ID, &out.UserName, &out.FullName, &out.Role)
	return
}
