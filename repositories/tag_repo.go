package repositories

import (
	"context"
	"database/sql"
	"fmt"

	"gitlab.com/rmsubekti/blogo/core/domain"
	"gitlab.com/rmsubekti/blogo/core/port"
)

type tagRepo struct {
	DB *sql.Tx
}

func NewTagRepo(db *sql.Tx) port.ITagRepo {
	return &tagRepo{DB: db}
}

func (r *tagRepo) GetByLabels(ctx context.Context, tags []string) (out []domain.Tag, err error) {
	var (
		labels string
		rows   *sql.Rows
	)

	for i, v := range tags {
		if i != 0 {
			labels += ","
		}
		labels += "'" + v + "'"
	}

	stmt := fmt.Sprintf("select id, label from tag where label in (%s)", labels)
	if rows, err = r.DB.QueryContext(ctx, stmt); err != nil {
		return
	}

	defer rows.Close()

	for rows.Next() {
		var tag domain.Tag
		if err = rows.Scan(&tag.ID, &tag.Label); err != nil {
			return
		}
		out = append(out, tag)
	}

	return
}
func (r *tagRepo) CreateTags(ctx context.Context, tags []domain.Tag) (err error) {
	stmn := "insert into tag(id,label) values "

	for i, v := range tags {
		if i != 0 {
			stmn += ","
		}
		stmn += fmt.Sprintf("('%s','%s')", v.ID, v.Label)
	}
	_, err = r.DB.ExecContext(ctx, stmn)
	return
}
func (r *tagRepo) GetByPostId(ctx context.Context, postId string) (out []domain.Tag, err error) {
	var rows *sql.Rows

	if rows, err = r.DB.QueryContext(ctx, `
		select t.id, t.label from tag t join post_tag p on p.tag_id = t.id
		where p.post_id = $1
	`, postId); err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		var tag domain.Tag
		if err = rows.Scan(&tag.ID, &tag.Label); err != nil {
			return
		}
		out = append(out, tag)
	}
	return
}

func (r *tagRepo) GetAll(ctx context.Context, in port.TagListRequest) (out []domain.Tag, err error) {
	var (
		rows *sql.Rows
		stmn = "select t.id, t.label from tag t "
	)

	if in.Limit < 1 {
		in.Limit = 10
	}

	if len(in.Search) > 0 {
		stmn += " where t.label ilike '%" + in.Search + "%' "
	}

	stmn += fmt.Sprintf("limit %d", in.Limit)

	if rows, err = r.DB.QueryContext(ctx, stmn); err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		var tag domain.Tag
		if err = rows.Scan(&tag.ID, &tag.Label); err != nil {
			return
		}
		out = append(out, tag)
	}
	return
}
