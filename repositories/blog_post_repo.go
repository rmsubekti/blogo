package repositories

import (
	"context"
	"database/sql"
	"fmt"
	"math"
	"strings"

	"gitlab.com/rmsubekti/blogo/core/constants"
	"gitlab.com/rmsubekti/blogo/core/domain"
	"gitlab.com/rmsubekti/blogo/core/port"
)

type blogPostRepo struct {
	DB *sql.Tx
}

func NewBlogPostRepo(db *sql.Tx) port.IBlogPostRepo {
	return &blogPostRepo{DB: db}
}

func (r *blogPostRepo) Create(ctx context.Context, in domain.BlogPost) (err error) {
	_, err = r.DB.ExecContext(ctx, `
		insert into blogpost(id, title, content, author,created_date) values($1,$2,$3,$4,$5);
	`, in.ID, in.Title, in.Content, in.Author, in.CreatedDate)
	return
}
func (r *blogPostRepo) Update(ctx context.Context, in domain.BlogPost) (err error) {
	_, err = r.DB.ExecContext(ctx, `update blogpost set title=$1, content=$2 where id=$3`, in.Title, in.Content, in.ID)
	return
}
func (r *blogPostRepo) IsOwner(ctx context.Context, userID, postID string) bool {
	var author string
	r.DB.QueryRowContext(ctx, "select author from blogpost where id=$1", postID).Scan(author)
	return strings.EqualFold(author, userID)
}
func (r *blogPostRepo) Delete(ctx context.Context, postId string) (err error) {
	_, err = r.DB.ExecContext(ctx, `delete from blogpost where id=$1`, postId)
	return
}

func (r *blogPostRepo) SetStatus(ctx context.Context, postId string, status constants.Status) (err error) {
	_, err = r.DB.ExecContext(ctx, `update  blogpost set status=$1, published_date=now() where id=$2`, status, postId)
	return
}

func (r *blogPostRepo) GetByID(ctx context.Context, id string) (out domain.BlogPost, err error) {
	err = r.DB.QueryRowContext(ctx, `select id, title, content, published_date, author, created_date from blogpost where id = $1`, id).
		Scan(&out.ID, &out.Title, &out.Content, &out.PublishedDate, &out.Author, &out.CreatedDate)
	return
}
func (r *blogPostRepo) List(ctx context.Context, in port.PostListRequest) (out port.PostListResponse, err error) {
	var (
		rows      *sql.Rows
		posts     []port.BlogPostListItem
		offset    uint
		status    = constants.STATUS_PUBLISHED
		order     = "asc"
		countStmn = "select count(b.id) from blogpost b "
		stmn      = "select b.id, b.title, left(b.content, 200) as headline, b.author from blogpost b "
		joinStmn  = "join post_tag p on p.post_id = b.id join tag t on t.id = p.tag_id "
	)

	if strings.EqualFold(in.Status, "draft") {
		status = constants.STATUS_DRAFT
	}
	joinStmn += fmt.Sprintf(`where b.status='%d' `, uint(status))

	if len(in.Search) > 0 {
		joinStmn += " and b.title ilike '%" + in.Search + "%' "
	}

	if len(in.Tag) > 0 {
		joinStmn += " and t.label ilike '%" + in.Tag + "%' "
	}
	countStmn += joinStmn
	stmn += joinStmn

	if err = r.DB.QueryRowContext(ctx, countStmn).Scan(&out.TotalRows); err != nil {
		return
	}

	if !strings.EqualFold("asc", in.Order) {
		order = "desc"
	}
	if in.Limit < 1 {
		in.Limit = 10
	}
	if in.Page < 1 {
		in.Page = 1
	}
	offset = (in.Page - 1) * in.Limit
	out.TotalPage = uint(math.Ceil(float64(out.TotalRows) / float64(in.Limit)))

	pagingStmn := fmt.Sprintf("order by b.created_date %s limit %d offset %d ", order, in.Limit, offset)

	//query rows
	if rows, err = r.DB.QueryContext(ctx, stmn+pagingStmn); err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		var post port.BlogPostListItem
		post.Author = &port.UserInfo{}
		if err = rows.Scan(&post.ID, &post.Title, &post.Headline, &post.Author.ID); err != nil {
			return
		}
		posts = append(posts, post)
	}
	out.PostListRequest = in
	out.Rows = &posts
	return
}
