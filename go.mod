module gitlab.com/rmsubekti/blogo

go 1.22.3

require (
	github.com/golang-jwt/jwt/v4 v4.5.0
	github.com/joho/godotenv v1.5.1
	github.com/lib/pq v1.10.9
	github.com/rs/cors v1.11.0
	github.com/segmentio/ksuid v1.0.4
	github.com/swaggo/http-swagger/v2 v2.0.2
	github.com/swaggo/swag v1.16.3
	github.com/xkok/gook v0.0.0-20240606145137-5dd13ae2d4a8
	golang.org/x/crypto v0.23.0
)

require (
	github.com/KyleBanks/depth v1.2.1 // indirect
	github.com/go-openapi/jsonpointer v0.21.0 // indirect
	github.com/go-openapi/jsonreference v0.21.0 // indirect
	github.com/go-openapi/spec v0.21.0 // indirect
	github.com/go-openapi/swag v0.23.0 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/swaggo/files/v2 v2.0.0 // indirect
	golang.org/x/tools v0.21.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
