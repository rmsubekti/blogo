FROM golang:1.22.3-alpine AS builder
RUN apk update && apk add --no-cache git
WORKDIR $GOPATH/src/blogo
COPY . .
RUN go install github.com/swaggo/swag/cmd/swag@latest
RUN go mod tidy
RUN swag init -d proto_rest -g server.go -o proto_rest/docs --parseDependency 
RUN mkdir /build
RUN cp .env.example /build/.env
RUN go build -o /build/blogo cmd/main.go


FROM alpine:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /rmsubekti
COPY --from=builder /build .
ENTRYPOINT [ "/rmsubekti/blogo" ]
