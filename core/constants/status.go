package constants

type Status uint

const STATUS_DRAFT Status = 0
const STATUS_PUBLISHED Status = 1
const STATUS_ACTIVE Status = 10
const STATUS_PRIMARY Status = 11
