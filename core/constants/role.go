package constants

type Role uint

const ROLE_ADMIN Role = 0
const ROLE_USER Role = 1
