package domain

type PostTag struct {
	ID     string
	PostId string
	TagId  string
}
