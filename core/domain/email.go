package domain

import (
	"time"

	"gitlab.com/rmsubekti/blogo/core/constants"
)

type Email struct {
	ID        string
	UID       string
	Email     string
	Status    constants.Status
	CreatedAt *time.Time
	DeletedAt *time.Time
}
