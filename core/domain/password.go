package domain

import "time"

type Password struct {
	ID        string
	UID       string
	Password  string
	CreatedAt *time.Time
	DeletedAt *time.Time
}
