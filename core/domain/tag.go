package domain

type Tag struct {
	ID    string
	Label string
}
