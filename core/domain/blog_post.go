package domain

import (
	"time"

	"gitlab.com/rmsubekti/blogo/core/constants"
)

type BlogPost struct {
	ID            string
	Title         string
	Content       string
	Status        constants.Status
	Author        string
	CreatedDate   *time.Time
	PublishedDate *time.Time
}
