package domain

import (
	"time"

	"gitlab.com/rmsubekti/blogo/core/constants"
)

type User struct {
	ID        string
	UserName  string
	FullName  string
	Role      constants.Role
	CreatedAt *time.Time
	DeletedAt *time.Time
}
