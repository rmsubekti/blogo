package port

import (
	"context"

	"gitlab.com/rmsubekti/blogo/core/domain"
)

type IPostTagRepo interface {
	GetAssociationByPostId(ctx context.Context, postId string) (associations []domain.PostTag, err error)
	CreateAssociations(ctx context.Context, assocs []domain.PostTag) (err error)
	DeleteAssociations(ctx context.Context, assocs []domain.PostTag) (err error)
}

type IPostTagCase interface {
	NewAssociation(ctx context.Context, postId string, tagId string) (out domain.PostTag, err error)
	CreateAssociations(ctx context.Context, postId string, tags []domain.Tag) (err error)
	UpdateAssociations(ctx context.Context, postId string, tags []domain.Tag) (err error)
}
