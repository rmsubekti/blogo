package port

import (
	"context"

	"gitlab.com/rmsubekti/blogo/core/domain"
)

type IPasswordRepo interface {
	SoftDelete(ctx context.Context, passId string) (err error)
	Create(ctx context.Context, pass domain.Password) (err error)
	GetAllByUID(ctx context.Context, uid string) ([]domain.Password, error)
}
type IPasswordCase interface {
	Create(ctx context.Context, uid, pass string) error
	Change(ctx context.Context, uid, pass string) error
}
