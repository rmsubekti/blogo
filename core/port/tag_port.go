package port

import (
	"context"

	"gitlab.com/rmsubekti/blogo/core/domain"
)

type ITagRepo interface {
	GetByLabels(ctx context.Context, tags []string) (out []domain.Tag, err error)
	CreateTags(ctx context.Context, tags []domain.Tag) error
	GetByPostId(ctx context.Context, postId string) (out []domain.Tag, err error)
	GetAll(ctx context.Context, in TagListRequest) (out []domain.Tag, err error)
}

type ITagCase interface {
	NewTag(ctx context.Context, tags string) (out domain.Tag, err error)
	Create(ctx context.Context, tags []string) (out []domain.Tag, err error)
	GetByPostId(ctx context.Context, postId string) (tags []domain.Tag, err error)
	GetAll(ctx context.Context, in TagListRequest) (out []domain.Tag, err error)
}

type TagListRequest struct {
	Limit  int    `json:"limit"`
	Search string `json:"search"`
}
