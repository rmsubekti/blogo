package port

import (
	"context"

	"gitlab.com/rmsubekti/blogo/core/constants"
	"gitlab.com/rmsubekti/blogo/core/domain"
)

type IBlogPostRepo interface {
	Create(ctx context.Context, in domain.BlogPost) error
	Update(ctx context.Context, in domain.BlogPost) error
	GetByID(ctx context.Context, id string) (domain.BlogPost, error)
	List(ctx context.Context, in PostListRequest) (out PostListResponse, err error)
	IsOwner(ctx context.Context, userID, postID string) bool
	SetStatus(ctx context.Context, postID string, status constants.Status) error
	Delete(ctx context.Context, postID string) error
}

type IBlogPostCase interface {
	WithTagRepo(tagRepo ITagRepo) IBlogPostCase
	WithPostTagRepo(postTagRepo IPostTagRepo) IBlogPostCase
	WithUserRepo(userRepo IUserRepo) IBlogPostCase
	Create(ctx context.Context, in BlogPostCreate) error
	Update(ctx context.Context, in BlogPostUpdate) error
	List(ctx context.Context, in PostListRequest) (out PostListResponse, err error)
	GetByID(ctx context.Context, id string) (out BlogPostView, err error)
	IsOwner(ctx context.Context, userID, postID string) bool
	SetStatus(ctx context.Context, postID string, status constants.Status) error
	Delete(ctx context.Context, postID string) error
}

type BlogPostCreate struct {
	UID     string    `json:"uid"`
	Title   string    `json:"title"`
	Content string    `json:"content"`
	Tags    *[]string `json:"tags"`
}

type BlogPostUpdate struct {
	ID string `json:"id"`
	BlogPostCreate
}
type PostListRequest struct {
	Page   uint   `json:"page"`
	Limit  uint   `json:"limit"`
	Order  string `json:"order"`
	Search string `json:"search"`
	Tag    string `json:"tag"`
	Status string `json:"status"`
}
type BlogPostListItem struct {
	ID       string        `json:"id"`
	Title    string        `json:"title"`
	Headline string        `json:"headline"`
	Author   *UserInfo     `json:"author"`
	Tags     *[]domain.Tag `json:"tags"`
}

type PostListResponse struct {
	PostListRequest
	TotalRows uint                `json:"total_rows"`
	TotalPage uint                `json:"total_page"`
	Rows      *[]BlogPostListItem `json:"rows,omitempty"`
}

type BlogPostView struct {
	ID      string        `json:"id"`
	Title   string        `json:"title"`
	Content string        `json:"headline"`
	Author  *UserInfo     `json:"author"`
	Tags    *[]domain.Tag `json:"tags"`
}
