package port

import (
	"context"

	"gitlab.com/rmsubekti/blogo/core/domain"
)

type IUserRepo interface {
	Create(ctx context.Context, user domain.User) error
	GetLoginInfo(ctx context.Context, username string) (out LoginInfo, err error)
	GetById(ctx context.Context, uid string) (out UserInfo, err error)
}

type IUserCase interface {
	WithPasswordRepo(passRepo IPasswordRepo) IUserCase
	WithEmailRepo(emailRepo IEmailRepo) IUserCase
	Register(ctx context.Context, in UserRegister) (err error)
	Login(ctx context.Context, in UserLogin) (out UserInfo, err error)
	GetById(ctx context.Context, uid string) (out UserInfo, err error)
}

type UserRegister struct {
	UserName string `json:"user_name"`
	FullName string `json:"full_name"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

type UserLogin struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type UserInfo struct {
	ID       string `json:"id"`
	UserName string `json:"user_name"`
	FullName string `json:"full_name"`
	Role     uint   `json:"role"`
}

type LoginInfo struct {
	UserInfo
	Password string
}
