package port

import (
	"context"

	"gitlab.com/rmsubekti/blogo/core/domain"
)

type IEmailRepo interface {
	Create(ctx context.Context, email domain.Email) error
	IsUsed(ctx context.Context, email string) bool
	GetLoginInfo(ctx context.Context, email string) (out LoginInfo, err error)
}
type IEmailCase interface {
	Create(ctx context.Context, uid, email string) (err error)
}
