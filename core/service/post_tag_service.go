package service

import (
	"context"
	"time"

	"github.com/segmentio/ksuid"
	"gitlab.com/rmsubekti/blogo/core/domain"
	"gitlab.com/rmsubekti/blogo/core/port"
)

type postTagCase struct {
	postTagRepo port.IPostTagRepo
}

func NewPostTagCase(postTagRepo port.IPostTagRepo) port.IPostTagCase {
	return &postTagCase{postTagRepo: postTagRepo}
}

func (c *postTagCase) NewAssociation(ctx context.Context, postId string, tagId string) (out domain.PostTag, err error) {
	var (
		id  ksuid.KSUID
		now = time.Now()
	)

	if id, err = ksuid.NewRandom(); err != nil {
		if id, err = ksuid.NewRandomWithTime(now); err != nil {
			return
		}
	}

	out = domain.PostTag{
		ID:     id.String(),
		PostId: postId,
		TagId:  tagId,
	}
	return
}

func (c *postTagCase) CreateAssociations(ctx context.Context, postId string, tags []domain.Tag) (err error) {
	var assocs []domain.PostTag

	for _, v := range tags {
		var ass domain.PostTag
		if ass, err = c.NewAssociation(ctx, postId, v.ID); err != nil {
			return
		}
		assocs = append(assocs, ass)
	}
	if err = c.postTagRepo.CreateAssociations(ctx, assocs); err != nil {
		return
	}
	return
}

func (c *postTagCase) UpdateAssociations(ctx context.Context, postId string, tags []domain.Tag) (err error) {
	var (
		old, rm, new []domain.PostTag
		found        bool
	)

	if old, err = c.postTagRepo.GetAssociationByPostId(ctx, postId); err != nil {
		return
	}

	for _, v := range old {
		found = false
		for _, t := range tags {
			if v.TagId == t.ID {
				found = true
				break
			}
		}
		if !found {
			rm = append(rm, v)
		}
	}
	if err = c.postTagRepo.DeleteAssociations(ctx, rm); err != nil {
		return
	}

	for _, v := range tags {
		found = false
		for _, o := range old {
			if v.ID == o.TagId {
				found = true
				break
			}
		}
		if !found {
			var ass domain.PostTag
			if ass, err = c.NewAssociation(ctx, postId, v.ID); err != nil {
				return
			}

			new = append(new, ass)
		}
	}
	if err = c.postTagRepo.CreateAssociations(ctx, new); err != nil {
		return
	}
	return
}
