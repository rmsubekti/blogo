package service

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/segmentio/ksuid"
	"github.com/xkok/gook"
	"gitlab.com/rmsubekti/blogo/core/constants"
	"gitlab.com/rmsubekti/blogo/core/domain"
	"gitlab.com/rmsubekti/blogo/core/port"
)

type emailCase struct {
	emailRepo port.IEmailRepo
}

func NewEmailCase(emailRepo port.IEmailRepo) port.IEmailCase {
	return &emailCase{emailRepo: emailRepo}
}

func (c *emailCase) Create(ctx context.Context, uid, email string) (err error) {
	var (
		now = time.Now()
		id  ksuid.KSUID
	)

	if _, err = ksuid.Parse(uid); err != nil {
		err = fmt.Errorf("invalid user id : %v", err)
		return
	}

	if err = gook.Email(email).Error(); err != nil {
		return
	}

	if c.emailRepo.IsUsed(ctx, email) {
		err = errors.New("email is already registered")
		return
	}

	if id, err = ksuid.NewRandom(); err != nil {
		if id, err = ksuid.NewRandomWithTime(now); err != nil {
			return
		}
	}

	mail := domain.Email{
		ID:        id.String(),
		UID:       uid,
		Email:     email,
		Status:    constants.STATUS_ACTIVE,
		CreatedAt: &now,
	}
	if err = c.emailRepo.Create(ctx, mail); err != nil {
		return
	}
	return
}
