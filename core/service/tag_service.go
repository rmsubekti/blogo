package service

import (
	"context"
	"strings"
	"time"

	"github.com/segmentio/ksuid"
	"gitlab.com/rmsubekti/blogo/core/domain"
	"gitlab.com/rmsubekti/blogo/core/port"
)

type tagCase struct {
	tagRepo port.ITagRepo
}

func NewTagCase(tagRepo port.ITagRepo) port.ITagCase {
	return &tagCase{tagRepo: tagRepo}
}

func (c *tagCase) NewTag(ctx context.Context, label string) (out domain.Tag, err error) {
	var (
		id  ksuid.KSUID
		now = time.Now()
	)

	if id, err = ksuid.NewRandom(); err != nil {
		if id, err = ksuid.NewRandomWithTime(now); err != nil {
			return
		}
	}
	out = domain.Tag{
		ID:    id.String(),
		Label: label,
	}

	return
}

func (c *tagCase) Create(ctx context.Context, labels []string) (out []domain.Tag, err error) {
	var newtags []domain.Tag

	if len(labels) < 1 {
		return
	}
	if out, err = c.tagRepo.GetByLabels(ctx, labels); err != nil {
		return nil, err
	}

	for _, l := range labels {
		found := false
		for _, t := range out {
			if strings.EqualFold(l, t.Label) {
				found = true
				break
			}
		}
		if !found {
			var tag domain.Tag
			if tag, err = c.NewTag(ctx, l); err != nil {
				return
			}
			newtags = append(newtags, tag)
		}
	}
	if len(newtags) > 0 {
		if err = c.tagRepo.CreateTags(ctx, newtags); err != nil {
			return
		}
	}

	out = append(out, newtags...)
	return
}

func (c *tagCase) GetByPostId(ctx context.Context, postId string) (tags []domain.Tag, err error) {
	tags, err = c.tagRepo.GetByPostId(ctx, postId)
	return
}

func (c *tagCase) GetAll(ctx context.Context, in port.TagListRequest) (tags []domain.Tag, err error) {
	tags, err = c.tagRepo.GetAll(ctx, in)
	return
}
