package service

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/segmentio/ksuid"
	"gitlab.com/rmsubekti/blogo/core/constants"
	"gitlab.com/rmsubekti/blogo/core/domain"
	"gitlab.com/rmsubekti/blogo/core/port"
)

type blogPostCase struct {
	blogPostRepo port.IBlogPostRepo
	postTagRepo  port.IPostTagRepo
	tagRepo      port.ITagRepo
	userRepo     port.IUserRepo
}

func NewBlogPostCase(bPostRepo port.IBlogPostRepo) port.IBlogPostCase {
	return &blogPostCase{blogPostRepo: bPostRepo}
}

func (c *blogPostCase) WithTagRepo(tagRepo port.ITagRepo) port.IBlogPostCase {
	c.tagRepo = tagRepo
	return c
}

func (c *blogPostCase) WithPostTagRepo(postTagRepo port.IPostTagRepo) port.IBlogPostCase {
	c.postTagRepo = postTagRepo
	return c
}

func (c *blogPostCase) WithUserRepo(userRepo port.IUserRepo) port.IBlogPostCase {
	c.userRepo = userRepo
	return c
}

func (c *blogPostCase) Create(ctx context.Context, in port.BlogPostCreate) (err error) {
	var (
		id   ksuid.KSUID
		now  = time.Now()
		tags []domain.Tag
	)
	if c.postTagRepo == nil {
		return errors.New("uninitialized post_tag repo : nil")
	}
	if c.tagRepo == nil {
		return errors.New("uninitialized tag repo : nil")
	}
	if len(in.Title) < 1 {
		return errors.New("required title")
	}
	if len(in.Content) < 1 {
		return errors.New("required content")
	}

	if _, err = ksuid.Parse(in.UID); err != nil {
		err = fmt.Errorf("invalid user id : %v", err)
		return
	}

	if id, err = ksuid.NewRandom(); err != nil {
		if id, err = ksuid.NewRandomWithTime(now); err != nil {
			return
		}
	}

	post := domain.BlogPost{
		ID:          id.String(),
		Title:       in.Title,
		Content:     in.Content,
		Status:      constants.STATUS_DRAFT,
		Author:      in.UID,
		CreatedDate: &now,
	}

	if err = c.blogPostRepo.Create(ctx, post); err != nil {
		return
	}

	if tags, err = NewTagCase(c.tagRepo).Create(ctx, *in.Tags); err != nil {
		return
	}

	if err = NewPostTagCase(c.postTagRepo).CreateAssociations(ctx, post.ID, tags); err != nil {
		return
	}

	return
}

func (c *blogPostCase) Update(ctx context.Context, in port.BlogPostUpdate) (err error) {
	var (
		post domain.BlogPost
		tags []domain.Tag
	)

	if c.postTagRepo == nil {
		return errors.New("uninitialized post_tag repo : nil")
	}
	if c.tagRepo == nil {
		return errors.New("uninitialized tag repo : nil")
	}

	if len(in.Title) < 1 {
		return errors.New("required title")
	}
	if len(in.Content) < 1 {
		return errors.New("required content")
	}

	if post, err = c.blogPostRepo.GetByID(ctx, in.ID); err != nil {
		return
	}

	if !strings.EqualFold(post.Author, in.UID) {
		return errors.New("not your post")
	}

	post.Title = in.Title
	post.Content = in.Content

	if err = c.blogPostRepo.Update(ctx, post); err != nil {
		return
	}

	if tags, err = NewTagCase(c.tagRepo).Create(ctx, *in.Tags); err != nil {
		return
	}

	if err = NewPostTagCase(c.postTagRepo).UpdateAssociations(ctx, post.ID, tags); err != nil {
		return
	}

	return
}

func (c *blogPostCase) Delete(ctx context.Context, postID string) error {
	return c.blogPostRepo.Delete(ctx, postID)
}

func (c *blogPostCase) SetStatus(ctx context.Context, postID string, status constants.Status) error {
	return c.blogPostRepo.SetStatus(ctx, postID, status)
}

func (c *blogPostCase) IsOwner(ctx context.Context, userID, postID string) bool {
	return c.blogPostRepo.IsOwner(ctx, userID, postID)
}

func (c *blogPostCase) List(ctx context.Context, in port.PostListRequest) (out port.PostListResponse, err error) {
	if c.tagRepo == nil {
		err = errors.New("uninitialized tag repo : nil")
		return
	}
	if c.userRepo == nil {
		err = errors.New("uninitialized user repo : nil")
		return
	}
	if out, err = c.blogPostRepo.List(ctx, in); err != nil {
		return
	}
	items := *out.Rows
	for i, v := range items {
		var (
			user port.UserInfo
			tags []domain.Tag
		)
		if user, err = c.userRepo.GetById(ctx, v.Author.ID); err != nil {
			return
		}
		if tags, err = c.tagRepo.GetByPostId(ctx, v.ID); err != nil {
			return
		}

		items[i].Author = &user
		items[i].Tags = &tags
	}

	out.Rows = &items
	return
}

func (c *blogPostCase) GetByID(ctx context.Context, id string) (out port.BlogPostView, err error) {
	var (
		post domain.BlogPost
		user port.UserInfo
		tags []domain.Tag
	)
	if c.tagRepo == nil {
		err = errors.New("uninitialized tag repo : nil")
		return
	}
	if c.userRepo == nil {
		err = errors.New("uninitialized user repo : nil")
		return
	}

	if post, err = c.blogPostRepo.GetByID(ctx, id); err != nil {
		return
	}

	if user, err = c.userRepo.GetById(ctx, post.Author); err != nil {
		return
	}
	if tags, err = c.tagRepo.GetByPostId(ctx, post.ID); err != nil {
		return
	}
	out = port.BlogPostView{
		ID:      post.ID,
		Title:   post.Title,
		Content: post.Content,
		Author:  &user,
		Tags:    &tags,
	}
	return
}
