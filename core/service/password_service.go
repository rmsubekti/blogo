package service

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/segmentio/ksuid"
	"github.com/xkok/gook"
	"gitlab.com/rmsubekti/blogo/core/domain"
	"gitlab.com/rmsubekti/blogo/core/port"
	"golang.org/x/crypto/bcrypt"
)

type passwordCase struct {
	passRepo port.IPasswordRepo
}

func NewPasswordCase(passRepo port.IPasswordRepo) port.IPasswordCase {
	return &passwordCase{passRepo: passRepo}
}

func (c *passwordCase) Create(ctx context.Context, uid, pass string) (err error) {
	var (
		hash []byte
		now  = time.Now()
		id   ksuid.KSUID
	)

	if _, err = ksuid.Parse(uid); err != nil {
		err = fmt.Errorf("invalid user id : %v", err)
		return
	}

	if err = gook.Password(pass).Min(8).Error(); err != nil {
		return
	}

	if hash, err = bcrypt.GenerateFromPassword([]byte(pass), bcrypt.DefaultCost); err != nil {
		return
	}

	if id, err = ksuid.NewRandom(); err != nil {
		if id, err = ksuid.NewRandomWithTime(now); err != nil {
			return
		}
	}

	password := domain.Password{
		ID:        id.String(),
		UID:       uid,
		Password:  string(hash),
		CreatedAt: &now,
	}

	if err = c.passRepo.Create(ctx, password); err != nil {
		return
	}
	return
}

func (c *passwordCase) Change(ctx context.Context, uid, pass string) (err error) {
	var (
		passwords []domain.Password
		current   string
	)

	if passwords, err = c.passRepo.GetAllByUID(ctx, uid); err != nil {
		return
	}

	for _, v := range passwords {
		if err = bcrypt.CompareHashAndPassword([]byte(v.Password), []byte(pass)); err == nil {
			return errors.New("you cant use the same password like you use before")
		}
		if v.DeletedAt == nil {
			current = v.ID
		}
	}

	if err = c.passRepo.SoftDelete(ctx, current); err != nil {
		return
	}

	if err = c.Create(ctx, uid, pass); err != nil {
		return
	}

	return
}
