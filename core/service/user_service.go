package service

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/segmentio/ksuid"
	"github.com/xkok/gook"
	"gitlab.com/rmsubekti/blogo/core/constants"
	"gitlab.com/rmsubekti/blogo/core/domain"
	"gitlab.com/rmsubekti/blogo/core/port"
	"golang.org/x/crypto/bcrypt"
)

type userCase struct {
	userRepo  port.IUserRepo
	passRepo  port.IPasswordRepo
	emailRepo port.IEmailRepo
}

func NewUserCase(userRepo port.IUserRepo) port.IUserCase {
	return &userCase{userRepo: userRepo}
}

func (c *userCase) WithPasswordRepo(passRepo port.IPasswordRepo) port.IUserCase {
	c.passRepo = passRepo
	return c
}
func (c *userCase) WithEmailRepo(emailRepo port.IEmailRepo) port.IUserCase {
	c.emailRepo = emailRepo
	return c
}

func (c *userCase) Register(ctx context.Context, in port.UserRegister) (err error) {
	var (
		uid ksuid.KSUID
		now = time.Now()
	)

	if c.emailRepo == nil {
		return errors.New("uninitialized email repo : nil")
	}

	if c.passRepo == nil {
		return errors.New("uninitialized password repo : nil")
	}

	if err = gook.UserName(in.UserName).Min(5).Error(); err != nil {
		return
	}

	if uid, err = ksuid.NewRandom(); err != nil {
		if uid, err = ksuid.NewRandomWithTime(now); err != nil {
			return
		}
	}

	user := domain.User{
		ID:        uid.String(),
		UserName:  in.UserName,
		FullName:  in.FullName,
		Role:      constants.ROLE_USER,
		CreatedAt: &now,
	}

	if err = c.userRepo.Create(ctx, user); err != nil {
		return
	}

	if err = NewEmailCase(c.emailRepo).Create(ctx, uid.String(), in.Email); err != nil {
		return
	}

	if err = NewPasswordCase(c.passRepo).Create(ctx, uid.String(), in.Password); err != nil {
		return
	}
	return
}
func (c *userCase) Login(ctx context.Context, in port.UserLogin) (out port.UserInfo, err error) {
	var (
		result  port.LoginInfo
		isEmail = gook.Email(in.Email).Ok()
		isUname = gook.UserName(in.Email).Ok()
	)
	if !isEmail && !isUname {
		err = fmt.Errorf("email or username not registered")
		return
	}

	if c.emailRepo == nil {
		err = errors.New("uninitialized email repo : nil")
		return
	}

	if isEmail {
		if result, err = c.emailRepo.GetLoginInfo(ctx, in.Email); err != nil {
			return
		}
	} else {
		if result, err = c.userRepo.GetLoginInfo(ctx, in.Email); err != nil {
			return
		}
	}

	if err = bcrypt.CompareHashAndPassword([]byte(result.Password), []byte(in.Password)); err != nil {
		err = fmt.Errorf("wrong password error: %v", err)
		return
	}
	out = result.UserInfo
	return
}

func (c *userCase) GetById(ctx context.Context, uid string) (out port.UserInfo, err error) {
	out, err = c.userRepo.GetById(ctx, uid)
	return
}
