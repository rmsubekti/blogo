package handlers

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/rmsubekti/blogo/core/constants"
	"gitlab.com/rmsubekti/blogo/core/port"
	"gitlab.com/rmsubekti/blogo/core/service"
	"gitlab.com/rmsubekti/blogo/pkg"
	"gitlab.com/rmsubekti/blogo/pkg/lib_pq"
	"gitlab.com/rmsubekti/blogo/repositories"
)

type blogPostHandler struct {
	*lib_pq.Conn
}

func NewBlogPostHandler() *blogPostHandler {
	return &blogPostHandler{Conn: &pkg.Conn}
}

// Create BlogPost godoc
// @Summary Create BlogPost
// @Description Create BlogPost
// @Tags         blogpost
// @Accept  json
// @Produce  json
// @Param request body port.BlogPostCreate true "Body payload"
// @Success 	200 {object} string
// @Failure      400  {object}  string
// @Failure      401  {object}  string
// @Failure      500  {object}  string
// @Router /posts [post]
// @Security Bearer
func (h *blogPostHandler) Create(w http.ResponseWriter, r *http.Request) {
	var (
		err          error
		body         []byte
		in           port.BlogPostCreate
		tx, _        = h.DB.Begin()
		user         = r.Context().Value("uinfo").(port.UserInfo)
		blogRepo     = repositories.NewBlogPostRepo(tx)
		postTagRepo  = repositories.NewPostTagRepo(tx)
		tagRepo      = repositories.NewTagRepo(tx)
		blogPostCase = service.NewBlogPostCase(blogRepo).WithPostTagRepo(postTagRepo).WithTagRepo(tagRepo)
	)
	defer tx.Rollback()

	if body, err = io.ReadAll(r.Body); err != nil {
		http.Error(w, "empty request body : "+err.Error(), http.StatusBadRequest)
		return
	}
	if err = json.Unmarshal(body, &in); err != nil {
		http.Error(w, "invalid request body : "+err.Error(), http.StatusBadRequest)
		return
	}
	in.UID = user.ID
	if err = blogPostCase.Create(r.Context(), in); err != nil {
		http.Error(w, "cannot create blog post : "+err.Error(), http.StatusInternalServerError)
		return
	}

	if err = tx.Commit(); err != nil {
		log.Println(err)
	}
	fmt.Fprint(w, "OK")
}

// Update a BlogPost godoc
// @Summary Update a BlogPost
// @Description Update a BlogPost
// @Tags         blogpost
// @Accept  json
// @Produce  json
// @Param        id   path      string  true  "BlogPost ID"
// @Param request body port.BlogPostUpdate true "Body payload"
// @Success 	200 {object} string
// @Failure      400  {object}  string
// @Failure      401  {object}  string
// @Failure      500  {object}  string
// @Router /posts/{id} [put]
// @Security Bearer
func (h *blogPostHandler) Update(w http.ResponseWriter, r *http.Request) {
	var (
		err          error
		body         []byte
		in           port.BlogPostUpdate
		tx, _        = h.DB.Begin()
		id           = r.PathValue("id")
		user         = r.Context().Value("uinfo").(port.UserInfo)
		blogRepo     = repositories.NewBlogPostRepo(tx)
		postTagRepo  = repositories.NewPostTagRepo(tx)
		tagRepo      = repositories.NewTagRepo(tx)
		blogPostCase = service.NewBlogPostCase(blogRepo).WithPostTagRepo(postTagRepo).WithTagRepo(tagRepo)
	)
	defer tx.Rollback()
	if body, err = io.ReadAll(r.Body); err != nil {
		http.Error(w, "empty request body : "+err.Error(), http.StatusBadRequest)
		return
	}

	if err = json.Unmarshal(body, &in); err != nil {
		http.Error(w, "invalid request body : "+err.Error(), http.StatusBadRequest)
		return
	}

	in.ID = id
	in.UID = user.ID

	if err = blogPostCase.Update(r.Context(), in); err != nil {
		http.Error(w, "cannot update blog post : "+err.Error(), http.StatusInternalServerError)
		return
	}
	tx.Commit()
	fmt.Fprint(w, "OK")
}

// List Blog Post godoc
// @Summary List Blog Post
// @Description admin only can see draft post
// @Tags         blogpost
// @Produce  json
// @Param        tag    query    string  false  "search by tag"
// @Param        status    query     string  false  "filter by status (draft/published) only admin see draft post"
// @Param        limit    query     int  false  "limit query"
// @Param        page    query     int  false  "page"
// @Param        order    query     int  false  "order asc/desc"
// @Success 200 {object} port.PostListResponse
// @Failure      400  {object}  string
// @Failure      401  {object}  string
// @Failure      500  {object}  string
// @Router /posts [get]
// @Security Bearer
func (h *blogPostHandler) List(w http.ResponseWriter, r *http.Request) {
	var (
		err          = r.ParseForm()
		in           port.PostListRequest
		tx, _        = h.DB.Begin()
		out          port.PostListResponse
		uinfo        = r.Context().Value("uinfo")
		status       = r.FormValue("status")
		tag          = r.FormValue("tag")
		search       = r.FormValue("search")
		page, _      = strconv.Atoi(r.FormValue("page"))
		limit, _     = strconv.Atoi(r.FormValue("limit"))
		order        = r.FormValue("order")
		isAdmin      = false
		blogRepo     = repositories.NewBlogPostRepo(tx)
		postTagRepo  = repositories.NewPostTagRepo(tx)
		tagRepo      = repositories.NewTagRepo(tx)
		userRepo     = repositories.NewUserRepo(tx)
		blogPostCase = service.NewBlogPostCase(blogRepo).WithPostTagRepo(postTagRepo).WithTagRepo(tagRepo).WithUserRepo(userRepo)
	)
	defer tx.Rollback()
	in.Page = uint(page)
	in.Limit = uint(limit)
	in.Tag = tag
	in.Search = search
	in.Status = status
	in.Order = order

	isDraft := strings.EqualFold(status, "draft")
	if uinfo != nil {
		user := uinfo.(port.UserInfo)
		isAdmin = (user.Role == uint(constants.ROLE_ADMIN))
	}

	if isDraft && !isAdmin {
		http.Error(w, "unauthorized ", http.StatusUnauthorized)
		return
	}

	if out, err = blogPostCase.List(r.Context(), in); err != nil {
		http.Error(w, "error : "+err.Error(), http.StatusInternalServerError)
		return
	}
	tx.Commit()
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(out)
}

// View a BlogPost godoc
// @Summary View a BlogPost
// @Description View a BlogPost
// @Tags         blogpost
// @Produce  json
// @Param        id   path      string  true  "BlogPost ID"
// @Success 	200 {object} port.BlogPostView
// @Failure      400  {object}  string
// @Failure      401  {object}  string
// @Failure      500  {object}  string
// @Router /posts/{id} [get]
// @Security Bearer
func (h *blogPostHandler) GetByID(w http.ResponseWriter, r *http.Request) {
	var (
		err          error
		id           = r.PathValue("id")
		tx, _        = h.DB.Begin()
		post         port.BlogPostView
		blogRepo     = repositories.NewBlogPostRepo(tx)
		blogPostCase = service.NewBlogPostCase(blogRepo)
	)
	defer tx.Rollback()
	if post, err = blogPostCase.GetByID(r.Context(), id); err != nil {
		http.Error(w, fmt.Sprintf("cannot retrieve post with id %s : %s", id, err.Error()), http.StatusInternalServerError)
		return
	}
	tx.Commit()
	json.NewEncoder(w).Encode(post)
}

// Publish a BlogPost godoc
// @Summary Publish a BlogPost
// @Description Publish a BlogPost
// @Tags         blogpost
// @Produce  json
// @Param        id   path      string  true  "BlogPost ID"
// @Param        status   path      string  true  "status to set"
// @Success 	200 {object} string
// @Failure      400  {object}  string
// @Failure      401  {object}  string
// @Failure      500  {object}  string
// @Router /posts/{id}/{status} [patch]
// @Security Bearer
func (h *blogPostHandler) SetStatus(w http.ResponseWriter, r *http.Request) {
	var (
		err          error
		id           = r.PathValue("id")
		status       = constants.STATUS_DRAFT
		user         = r.Context().Value("uinfo").(port.UserInfo)
		tx, _        = h.DB.Begin()
		blogRepo     = repositories.NewBlogPostRepo(tx)
		blogPostCase = service.NewBlogPostCase(blogRepo)
	)
	defer tx.Rollback()
	if user.Role != uint(constants.ROLE_ADMIN) {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}
	if !strings.EqualFold(r.PathValue("status"), "draft") {
		status = constants.STATUS_PUBLISHED
	}
	if err = blogPostCase.SetStatus(r.Context(), id, status); err != nil {
		http.Error(w, "cannot publish blog post : "+err.Error(), http.StatusInternalServerError)
		return
	}
	tx.Commit()
	fmt.Fprint(w, "OK")
}

// Delete a BlogPost godoc
// @Summary Delete a BlogPost
// @Description Delete a BlogPost
// @Tags         blogpost
// @Produce  json
// @Param        id   path      string  true  "BlogPost ID"
// @Success 	200 {object} string
// @Failure      400  {object}  string
// @Failure      401  {object}  string
// @Failure      500  {object}  string
// @Router /posts/{id} [delete]
// @Security Bearer
func (h *blogPostHandler) Delete(w http.ResponseWriter, r *http.Request) {
	var (
		err          error
		id           = r.PathValue("id")
		user         = r.Context().Value("uinfo").(port.UserInfo)
		tx, _        = h.DB.Begin()
		blogRepo     = repositories.NewBlogPostRepo(tx)
		blogPostCase = service.NewBlogPostCase(blogRepo)
	)
	defer tx.Rollback()
	if !blogPostCase.IsOwner(r.Context(), user.ID, id) && user.Role != uint(constants.ROLE_ADMIN) {
		http.Error(w, "unauthorized", http.StatusUnauthorized)
		return
	}

	if err = blogPostCase.Delete(r.Context(), id); err != nil {
		http.Error(w, "cannot delete blog post : "+err.Error(), http.StatusInternalServerError)
		return
	}
	tx.Commit()
	fmt.Fprint(w, "OK")
}
