package handlers

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"

	"gitlab.com/rmsubekti/blogo/core/port"
	"gitlab.com/rmsubekti/blogo/core/service"
	"gitlab.com/rmsubekti/blogo/pkg"
	"gitlab.com/rmsubekti/blogo/pkg/auth_jwt"
	"gitlab.com/rmsubekti/blogo/pkg/lib_pq"
	"gitlab.com/rmsubekti/blogo/repositories"
)

type userHandler struct {
	*lib_pq.Conn
}

func NewUserhandler() *userHandler {
	return &userHandler{Conn: &pkg.Conn}
}

// Register godoc
// @Summary Register a new account
// @Description Usename cannot be changed after account is created
// @Tags         account
// @Produce  json
// @Param request body port.UserRegister true "Body payload"
// @Success 200 {string} ok
// @Failure      400  {object}  string
// @Failure      401  {object}  string
// @Failure      500  {object}  string
// @Router /register [post]
func (h *userHandler) Register(w http.ResponseWriter, r *http.Request) {
	var (
		err      error
		body     []byte
		in       port.UserRegister
		tx, _    = h.DB.Begin()
		userRepo = repositories.NewUserRepo(tx)
		mailRepo = repositories.NewEmailRepo(tx)
		passRepo = repositories.NewPasswordRepo(tx)
		userCase = service.NewUserCase(userRepo).WithEmailRepo(mailRepo).WithPasswordRepo(passRepo)
	)
	defer tx.Rollback()
	if body, err = io.ReadAll(r.Body); err != nil {
		http.Error(w, "empty request body : "+err.Error(), http.StatusBadRequest)
		return
	}

	if err = json.Unmarshal(body, &in); err != nil {
		http.Error(w, "invalid request body : "+err.Error(), http.StatusBadRequest)
		return
	}

	if err = userCase.Register(r.Context(), in); err != nil {
		http.Error(w, "error registering new account  : "+err.Error(), http.StatusInternalServerError)
		return
	}

	if err = tx.Commit(); err != nil {
		log.Println(err)
	}
	fmt.Fprint(w, "OK")
}

// Login godoc
// @Summary Login
// @Description Create new session
// @Description email value can be email or username
// @Tags         account
// @Produce  json
// @Param request body string true " Body payload message/rfc822" SchemaExample({\n\t"email": "superuser",\n\t"password": "5uper@Secret"\n})
// @Success 200 {object} port.UserInfo
// @Failure      400  {object}  string
// @Failure      401  {object}  string
// @Failure      500  {object}  string
// @Router /login [post]
func (h *userHandler) Login(w http.ResponseWriter, r *http.Request) {
	var (
		err      error
		body     []byte
		token    string
		in       port.UserLogin
		out      port.UserInfo
		tx, _    = h.DB.Begin()
		userCase = service.NewUserCase(repositories.NewUserRepo(tx)).
				WithEmailRepo(repositories.NewEmailRepo(tx))
	)
	defer tx.Rollback()

	if body, err = io.ReadAll(r.Body); err != nil {
		http.Error(w, "empty request body : "+err.Error(), http.StatusBadRequest)
		return
	}

	if err = json.Unmarshal(body, &in); err != nil {
		http.Error(w, "invalid request body : "+err.Error(), http.StatusBadRequest)
		return
	}

	if out, err = userCase.Login(r.Context(), in); err != nil {
		http.Error(w, "error login  : "+err.Error(), http.StatusInternalServerError)
		return
	}
	if token, err = auth_jwt.NewToken(&out, 2); err != nil {
		http.Error(w, "error generating token  : "+err.Error(), http.StatusInternalServerError)
		return
	}
	tx.Commit()
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]any{"user": out, "token": token})
}

func (h *userHandler) GetUserInfo(w http.ResponseWriter, r *http.Request) {
	var (
		err       error
		out, user port.UserInfo
		tx, _     = h.DB.Begin()
		userRepo  = repositories.NewUserRepo(tx)
		userCase  = service.NewUserCase(userRepo)
		uinfo     = r.Context().Value("uinfo")
	)
	defer tx.Rollback()
	if uinfo == nil {
		http.Error(w, "user not found", http.StatusNotImplemented)
		return
	}
	user = uinfo.(port.UserInfo)
	log.Println(user)

	if out, err = userCase.GetById(r.Context(), user.ID); err != nil {
		http.Error(w, "error getting user info  : "+err.Error(), http.StatusInternalServerError)
		return
	}
	tx.Commit()
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(out)
}
