package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/rmsubekti/blogo/core/domain"
	"gitlab.com/rmsubekti/blogo/core/port"
	"gitlab.com/rmsubekti/blogo/core/service"
	"gitlab.com/rmsubekti/blogo/pkg"
	"gitlab.com/rmsubekti/blogo/pkg/lib_pq"
	"gitlab.com/rmsubekti/blogo/repositories"
)

type tagHandler struct {
	*lib_pq.Conn
}

func NewTagHandler() *tagHandler {
	return &tagHandler{Conn: &pkg.Conn}
}

// List tags godoc
// @Summary List tags
// @Description List tags
// @Tags         tags
// @Produce  json
// @Param        search    query     string  false  "query search"
// @Param        limit    query     int  false  "result limit"
// @Success 200 {object} []domain.Tag
// @Failure      400  {object}  string
// @Failure      401  {object}  string
// @Failure      500  {object}  string
// @Router /tags [get]
func (h *tagHandler) List(w http.ResponseWriter, r *http.Request) {
	var (
		err      = r.ParseForm()
		in       port.TagListRequest
		out      []domain.Tag
		search   = r.FormValue("limit")
		limit, _ = strconv.Atoi(r.FormValue("limit"))
		tx, _    = h.DB.Begin()
		tagRepo  = repositories.NewTagRepo(tx)
		tagCase  = service.NewTagCase(tagRepo)
	)
	in.Search = search
	in.Limit = limit
	defer tx.Rollback()
	if out, err = tagCase.GetAll(r.Context(), in); err != nil {
		http.Error(w, fmt.Sprintf("cannot retrieve tag list : %s ", err.Error()), http.StatusInternalServerError)
		return
	}

	tx.Commit()
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(out)
}
