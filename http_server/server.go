package http_server

import (
	"log"
	"net/http"

	"github.com/rs/cors"
	httpSwagger "github.com/swaggo/http-swagger/v2"
	"gitlab.com/rmsubekti/blogo/http_server/docs"
	"gitlab.com/rmsubekti/blogo/http_server/middleware"
	"gitlab.com/rmsubekti/blogo/http_server/router"
	"gitlab.com/rmsubekti/blogo/pkg"
)

// @title  Blogo
// @version 1.0
// @description A simple blog rest api.
// @termsOfService http://rmsubekti.github.io

// @contact.name API Support
// @contact.url http://rmsubekti.github.io
// @contact.email  rmsubekti2011@gmail.com

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @BasePath /api
// @securityDefinitions.apikey Bearer
// @in header
// @name Authorization
// @description Type "Bearer" followed by a space and JWT token.
func Serve() {
	var (
		host = pkg.Env.HOSTNAME
		port = pkg.Env.PORT
	)
	docs.SwaggerInfo.Host = host + ":" + port
	mux := http.NewServeMux()
	router.BlogPostRouter(mux)
	router.UserRouter(mux)
	router.TagRouter(mux)
	mux.Handle("GET /swagger/", httpSwagger.Handler(
		httpSwagger.URL("/swagger/doc.json"),                        // The url pointing to API definition
		httpSwagger.DefaultModelsExpandDepth(httpSwagger.HideModel), // Models will not be expanded
	))
	m := middleware.Auth(mux)
	m = cors.AllowAll().Handler(m)

	log.Printf("server running on : http://%s:%s]\n", host, port)
	log.Printf("API documentation served on : http://%s:%s/swagger\n", host, port)

	if err := http.ListenAndServe(":"+port, m); err != nil {
		log.Fatal(err)
	}
}
