package router

import (
	"net/http"

	"gitlab.com/rmsubekti/blogo/http_server/handlers"
)

func UserRouter(mux *http.ServeMux) {
	userHandler := handlers.NewUserhandler()
	mux.HandleFunc("POST /api/register", userHandler.Register)
	mux.HandleFunc("POST /api/login", userHandler.Login)
	mux.HandleFunc("GET /api/user", userHandler.GetUserInfo)

}
