package router

import (
	"net/http"

	"gitlab.com/rmsubekti/blogo/http_server/handlers"
)

func TagRouter(mux *http.ServeMux) {
	tagHandler := handlers.NewTagHandler()
	mux.HandleFunc("GET /api/tags", tagHandler.List)

}
