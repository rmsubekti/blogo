package router

import (
	"net/http"

	"gitlab.com/rmsubekti/blogo/http_server/handlers"
)

func BlogPostRouter(mux *http.ServeMux) {
	blogPost := handlers.NewBlogPostHandler()
	mux.HandleFunc("POST /api/posts", blogPost.Create)
	mux.HandleFunc("PUT /api/posts/{id}", blogPost.Update)
	mux.HandleFunc("GET /api/posts", blogPost.List)
	mux.HandleFunc("DELETE /api/posts/{id}", blogPost.Delete)
	mux.HandleFunc("GET /api/posts/{id}", blogPost.GetByID)
	mux.HandleFunc("PATCH /api/posts/{id}/{status}", blogPost.SetStatus)
}
