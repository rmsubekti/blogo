package middleware

import (
	"context"
	"net/http"
	"strings"

	"gitlab.com/rmsubekti/blogo/pkg/auth_jwt"
)

func Auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var (
			path  = r.URL.Path
			token = r.Header.Get("Authorization")
			user  any
			err   error
		)
		if strings.Contains(path, "login") || strings.Contains(path, "register") {
			next.ServeHTTP(w, r)
			return
		}

		if token == "" && r.Method == http.MethodGet {
			next.ServeHTTP(w, r)
			return
		}

		if user, err = auth_jwt.Verify(token); err != nil {
			http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
			return
		}

		ctx := context.WithValue(r.Context(), "uinfo", user)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
