## Running this app

1. Using `make` tool
    ```
    make docs && make run
    ```

 if `make` not installed in your machine just open [`Makefile`](https://gitlab.com/rmsubekti/blogo/-/blob/master/Makefile) to see the command 
## Run using prebuild docker images 

1. create docker `compose.yml` file

2. in compose.yml file 
    ```
    version: "3.9"

    services:
    db:
        image: postgres:latest
        restart: always
        environment:
            POSTGRES_DB: postgres
            POSTGRES_PASSWORD: blogo-00BC7ddAa54cpB*

    blogo:
        image: rmsubekti/blogo:master
        restart: unless-stopped
        environment:
            POSTGRES_DB: postgres # Same as postgres environment
            POSTGRES_PASSWORD: blogo-00BC7ddAa54cpB* # Same as postgres environment
            POSTGRES_HOSTNAME: db
            HOSTNAME: localhost #192.168.192.199 or domain.com
            PORT=80
        ports:
        - 80:80
        depends_on:
        - db
    ```

    > **change ports as you like** to access it on your local machine

3. save and run:
    ```
    docker compose up -d
    ```

4. open Swagger API documentation on

```
    http://localhost:80/swagger
```


### Notes
- user can login with email or username. default admin account :
    ```
    username : superuser
    email : admin@email.com
    password : 5uper@Secret
    ```
    example is provided on swagger ui path:`/swagger` 

- Use token for authentication using prefix `Bearer ` then append it with the token, separated with a space. example:
    ```
    Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIwIiwic3ViIjoic3VwZXJ1c2VyIiwiZXhwIjoxNzE4MjU4MjU1LCJpYXQiOjE3MTgwODU0NTUsImp0aSI6IjJoTTZSaTlwZUtnTmRjUFpKUkxiNXRTSUdVVyJ9.i9p0_NJk4
    ```
- admin can see all draft post by adding url parameter to post list 
    ```
    host/api/posts?status=draft
    ```
    without this parameter, only show public/ published post
