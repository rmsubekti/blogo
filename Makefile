.PHONY: docs 
docs:
	swag init -d proto_rest -g server.go -o proto_rest/docs --parseDependency 

run:
	docker start blogo || docker run --name blogo -e POSTGRES_DB=blogo -e  POSTGRES_PASSWORD=blogo-00BC7ddAa54cpB -e POSTGRES_USER=blogo -p 5432:5432 -d postgres \
	&& go run cmd/main.go 
